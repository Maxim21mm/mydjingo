import logging

logging.basicConfig(filename='log.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


# def changelog():
#     log = logging.getLogger()
#     log.setLevel(logging.CRITICAL)


# def decor(dry_run=False):
def logger(func):
    def wrapper(*args, **kwargs):
        try:
            logging.info(func(*args, **kwargs))
            # logging.info("function is %s, with args %s and kwargs %s", func.__name__, args[1:], kwargs)
        except Exception as e:
            logging.error(e)
    return wrapper
    # return logger


    # for_restore.sort()
    #
    # prev_file = ''
    # prev_old_file_path = ''
    #
    # for file_for_rest in for_restore:
    #
    #     equals_files.append(prev_file)
    #
    #     file_name = os.path.join(self.INFO_PATH, file_for_rest)
    #     with open(file_name, "r") as file_info:
    #         old_file_path = file_info.readline()
    #
    #     if prev_file != '':
    #         prev_file_name = os.path.join(self.INFO_PATH, prev_file)
    #         with open(prev_file_name, "r") as file_info:
    #             prev_old_file_path = file_info.readline()
    #
    #     if old_file_path == prev_old_file_path:
    #         equals_files.append(file_for_rest)
    #     else:
    #         self.restore_file(file_for_rest)
    #     prev_file = file_for_rest

    # l = len(file_or_dir)
    # i = 0
    #
    # while i<l - 1:
    #     equals_files = ''
    #     j = 0
    #     equals_files.append(file_or_dir[i])
    #     file_i = os.path.join(self.INFO_PATH, file_or_dir[i])
    #     with open(file_i, "r") as file_info:
    #         i_path = file_info.readline()
    #     while j < l:
    #
    #         file_j = os.path.join(self.INFO_PATH, file_or_dir[j])
    #         with open(file_j, "r") as file_info:
    #             j_path = file_info.readline()
    #
    #         if i
    #         files_or_dirs