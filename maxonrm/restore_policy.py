import os
import shutil
from maxonrm.ver import Checker
from chose import Chose


class SomePolicy(object):

    @staticmethod
    def restore_ask(old_path, new_path):
        # if os.path.exists(new_path):
        #     # print "file already created. press Y or N for replace"
        #     # if Chose.question():
        #     return new_path
        # else:
        shutil.move(old_path, new_path)

    @staticmethod
    def replace(old_path, new_path):
        folders, file_name = os.path.split(new_path)
        if os.path.exists(new_path):
            i = 1
            while Checker.check_file(new_path, "{0}.{1}".format(file_name, i)):
                i += 1
            file_name = "{0}.{1}".format(file_name, i)

        new_path = os.path.join(folders, file_name)
        shutil.move(old_path, new_path)


class RestorePolicy(object):

    @staticmethod
    def restore_by_policy(old_path, new_path, policy):
        if policy == 'replace':
            SomePolicy.replace(old_path, new_path)
        elif policy == 'ask':
            SomePolicy.restore_ask(old_path, new_path)
