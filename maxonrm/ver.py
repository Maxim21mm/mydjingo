import os
import shutil
import re


class Checker(object):

    @staticmethod
    def check_file(trash_path, file_name):
        path = os.path.join(trash_path, file_name)
        return os.path.exists(path)

    @staticmethod
    def check_secure(file_path):
        if os.path.isdir(file_path):
            return os.access(file_path, os.W_OK)
        else:
            return True

    @staticmethod
    def in_trash(trash_path, file_path):
        folders = os.path.split(file_path)[0]
        if folders == trash_path:
            print ("%s already in trash" % os.path.split(file_path)[1])
            return True
        else:
            return False

    @staticmethod
    def contain_trash(trash_path, file_path):
        if os.path.isdir(file_path) and re.match(file_path, trash_path):
            return True
        else:
            return False

    @staticmethod
    def trash_check(trash_path, file_path):
        first_check = Checker.in_trash(trash_path, file_path)
        second_check = Checker.contain_trash(trash_path, file_path)
        if first_check or second_check:
            return True
        else:
            return False

    @staticmethod
    def update(trash_path):
        files_path = os.path.join(trash_path, "files")
        info_path = os.path.join(trash_path, "info")
        list_files = os.listdir(files_path)
        list_info = os.listdir(info_path)
        checked_list = []

        for trash_file in list_files:
            for info_file in list_info:
                if trash_file + ".file_info" == info_file:
                    checked_list.append(trash_file)

        for trash_file in list_files:
            tmp = 0
            for checked in checked_list:
                if trash_file == checked:
                    tmp = 1
            if tmp == 0:
                if os.path.isdir(os.path.join(files_path, trash_file)):
                    print ("this directory lost his info. Directory %s deleted from trash" % trash_file)
                    shutil.rmtree(os.path.join(files_path, trash_file))
                else:
                    print ("this file lost his info. File %s deleted from trash" % trash_file)
                    os.remove(os.path.join(files_path, trash_file))

        for trash_info in list_info:
            tmp = 0
            for checked in checked_list:
                if trash_info == checked + ".file_info":
                    tmp = 1
            if tmp == 0:
                print ("this info lost his file or directory. File_info %s deleted from INFO" % trash_info)
                os.remove(os.path.join(info_path, trash_info))

