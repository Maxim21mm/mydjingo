from unittest import TestCase
from trash import Trash
import os
import shutil
import  tempfile


class TestTrash(TestCase):

    def setUp(self):
        self.path = tempfile.mkdtemp()
        self.trash = Trash(self.path, 15, 15, 'size', 'force', '1221', 'replace')
        self.trash_path = self.trash.TRASH_PATH
        self.file_path = self.trash.FILES_PATH
        self.info_path = self.trash.INFO_PATH

    def tearDown(self):
        shutil.rmtree(self.path)

    def test_make_dir(self):
        self.trash.make_dir(self.trash.FILES_PATH)
        self.assertTrue(os.path.exists(self.trash.FILES_PATH))

    def test_delete_file(self):
        file_ = tempfile.NamedTemporaryFile()

        folders, file_name = os.path.split(file_.name)
        self.trash.delete_file(folders, file_name)

        self.assertTrue(os.path.exists(os.path.join(self.file_path, file_name)))
        self.assertTrue(os.path.exists(os.path.join(self.info_path, file_name + ".file_info")))
        self.assertFalse(os.path.exists(file_.name))

    def test_restore_file(self):
        file_ = tempfile.NamedTemporaryFile(dir=self.file_path)
        file_name = os.path.split(file_.name)[1]

        info = self.info_path + "/" + file_name + ".file_info"
        old_dir = tempfile.mkdtemp()

        with open(info, "w") as infos:
            infos.write(old_dir + "/" + file_name)

        self.trash.restore_file(file_name)

        self.assertTrue(os.path.exists(os.path.join(old_dir, file_name)))
        self.assertFalse(os.path.exists(file_.name))
        self.assertFalse(os.path.exists(info))

    def test_remove_from_trash(self):
        file_ = tempfile.NamedTemporaryFile(dir=self.file_path)
        print file_.name
        file_name = os.path.split(file_.name)[1]
        info = self.info_path + "/" + file_name + ".file_info"
        print info
        with open(info, "w") as infos:
            infos.write("asdasd")

        self.trash.remove_from_trash(file_name)

        self.assertFalse(os.path.exists(file_name))
        self.assertFalse(os.path.exists(info))

    def test_clean_trash(self):
        file_ = tempfile.NamedTemporaryFile(dir=self.file_path)
        print file_.name
        file_name = os.path.split(file_.name)[1]
        info = self.info_path + "/" + file_name + ".file_info"
        print info
        with open(info, "w") as infos:
            infos.write("asdasd")

        self.trash.clean_trash()

        files_list = os.listdir(self.file_path)
        info_list = os.listdir(self.info_path)
        self.assertTrue(len(files_list) == 0)
        self.assertTrue(len(info_list) == 0)

    def test_remove_by_re(self):

        dir_ = tempfile.mkdtemp()
        file_1 = tempfile.NamedTemporaryFile(prefix="aaaAA", dir=dir_)
        file1 = os.path.split(file_1.name)[1]
        file_2 = tempfile.NamedTemporaryFile(prefix="aaD", dir=dir_)
        file2 = os.path.split(file_2.name)[1]
        file_3 = tempfile.NamedTemporaryFile(prefix="aaaAA", dir=dir_)
        file3 = os.path.split(file_3.name)[1]
        file_4 = tempfile.NamedTemporaryFile(dir=dir_)
        file4 = os.path.split(file_4.name)[1]

        self.trash.remove_by_re(dir_, "aaaAA")

        self.assertTrue(os.path.exists(os.path.join(self.file_path, file1)))
        self.assertTrue(os.path.exists(os.path.join(self.info_path, file1 + ".file_info")))

        self.assertTrue(os.path.exists(os.path.join(self.file_path, file3)))
        self.assertTrue(os.path.exists(os.path.join(self.info_path, file3 + ".file_info")))

        self.assertFalse(os.path.exists(file_1.name))
        self.assertTrue(os.path.exists(file_2.name))
        self.assertFalse(os.path.exists(file_3.name))
        self.assertTrue(os.path.exists(file_4.name))

    def test_restore_by_re(self):

        file_1 = tempfile.NamedTemporaryFile(prefix="aaaAA", dir=self.file_path)
        file1 = os.path.split(file_1.name)[1]
        file_2 = tempfile.NamedTemporaryFile(prefix="aaD", dir=self.file_path)
        file2 = os.path.split(file_2.name)[1]
        file_3 = tempfile.NamedTemporaryFile(prefix="aaaAA", dir=self.file_path)
        file3 = os.path.split(file_3.name)[1]
        file_4 = tempfile.NamedTemporaryFile(dir=self.file_path)
        file4 = os.path.split(file_4.name)[1]

        old_dir = tempfile.mkdtemp()

        info1 = self.info_path + "/" + file1 + ".file_info"
        with open(info1, "w") as infos:
            infos.write(old_dir + "/" + file1)

        info2 = self.info_path + "/" + file2 + ".file_info"
        with open(info2, "w") as infos:
            infos.write(old_dir + "/" + file2)

        info3 = self.info_path + "/" + file3 + ".file_info"
        with open(info3, "w") as infos:
            infos.write(old_dir + "/" + file3)

        info4 = self.info_path + "/" + file4 + ".file_info"
        with open(info4, "w") as infos:
            infos.write(old_dir + "/" + file4)

        self.trash.restore_by_re("aaaAA")

        self.assertTrue(os.path.exists(os.path.join(old_dir, file1)))
        self.assertTrue(os.path.exists(os.path.join(old_dir, file3)))

        self.assertFalse(os.path.exists(file_1.name))
        self.assertFalse(os.path.exists(os.path.join(self.info_path, file1 + ".file_info")))

        self.assertFalse(os.path.exists(file_3.name))
        self.assertFalse(os.path.exists(os.path.join(self.info_path, file3 + ".file_info")))

        self.assertFalse(os.path.exists(file_1.name))
        self.assertTrue(os.path.exists(file_2.name))
        self.assertFalse(os.path.exists(file_3.name))
        self.assertTrue(os.path.exists(file_4.name))
