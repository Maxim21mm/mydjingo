import os
import shutil
import datetime
from chose import Chose
from ver import Checker
from restore_policy import RestorePolicy


def simple_del(file_name, path, trash=None):
    if Checker.check_file(trash.FILES_PATH, file_name):
        i = 1
        while Checker.check_file(trash.FILES_PATH, "{0}.{1}".format(file_name, i)):
            i += 1
        file_name = "{0}.{1}".format(file_name, i)

    new_path = os.path.join(trash.FILES_PATH, file_name)
    with open(os.path.join(trash.INFO_PATH, file_name + ".file_info"), "w") as file_info:
        file_info.write(os.path.join(path) + "\n")
        file_info.write(str(datetime.datetime.now()))

    shutil.move(path, new_path)


def force_del(file_name, path, trash=None):
    if not trash.DRY_RUN_MOD:
        simple_del(file_name, path, trash)


def enteractive_del(file_name, path, trash):
    print 'delete %s from %s' % (file_name, path)
    if Chose.question():
        if not trash.DRY_RUN_MOD:
            simple_del(file_name, path, trash)


def run_del(file_name, path, trash=None):
    if trash.MODE == 'force':
        force_del(file_name, path, trash)
    elif trash.MODE == 'interactive':
        enteractive_del(file_name, path, trash)


def simple_restore(file_name, new_path, old_path, trash=None):
    RestorePolicy.restore_by_policy(old_path, new_path, trash.RESTORE_POLICY)
    os.remove(os.path.join(trash.INFO_PATH, file_name + ".file_info"))


def force_restore(file_name, new_path, old_path, trash=None):
    if not trash.DRY_RUN_MOD:
        simple_restore(file_name, new_path, old_path, trash)


def enteractive_restore(file_name, new_path, old_path, trash):
    print "restore %s in %s" % (file_name, new_path)
    if Chose.question():
        if not trash.DRY_RUN_MOD:
            simple_restore(file_name, new_path, old_path, trash)


def run_rest(file_name, new_path, old_path, trash=None):
    if trash.MODE == 'force':
        force_restore(file_name, new_path, old_path, trash)
    elif trash.MODE == 'interactive':
        enteractive_restore(file_name, new_path, old_path, trash)

