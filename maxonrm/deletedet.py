import os
import datetime
import shutil
from ver import Checker
import logging


class Deleted(object):

    @staticmethod
    def remove_simple(dir_path, files, trash=None):

        if Checker.check_combine(dir_path, files, trash):
            return None

        for file_name in files:
            folders, file_name = os.path.split(file_name)
            if folders == '':
                path = os.path.join(dir_path, file_name)
            else:
                path = os.path.join(folders, file_name)
            if os.path.lexists(path):
                if not Checker.trash_check(trash.FILES_PATH, path):
                    logging.info("file %s successful deleted", file_name)
                    if trash.DRY_RUN_MOD:
                        pass
                    else:
                        if Checker.check_file(trash.FILES_PATH, file_name):
                            i = 1
                            while Checker.check_file(trash.FILES_PATH, "{0}.{1}".format(file_name, i)):
                                i += 1
                            file_name = "{0}.{1}".format(file_name, i)

                        new_path = os.path.join(trash.FILES_PATH, file_name)
                        with open(os.path.join(trash.INFO_PATH, file_name + ".file_info"), "w") as file_info:
                            file_info.write(os.path.join(path) + "\n")
                            file_info.write(str(datetime.datetime.now()))

                        shutil.move(path, new_path)

                else:
                    if trash.SILENT_MOD:
                        pass
                    else:
                        logging.error("file %s is already in trash", file_name)
                        # raise Exception
            else:
                if trash.SILENT_MOD:
                    pass
                else:
                    logging.error("Don't found file or directory %s", file_name)
                    # raise OSError
