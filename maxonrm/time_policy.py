import os


class TimePolicy(object):

    @staticmethod
    def delete_by_time(trash, count):
        file_list = []
        trash_files_and_dirs = os.listdir(trash.FILES_PATH)
        for file_or_dir in trash_files_and_dirs:
            with open(os.path.join(trash.INFO_PATH, file_or_dir + ".file_info"), "r") as file_info:
                new_path = file_info.readline().strip("\n")
                time = file_info.readline()
            file_list.append([time,  file_or_dir])
        file_list.sort()
        try:
            for i in xrange(count):
                trash.remove_from_trash(file_list[i][1])
        except IndexError:
            pass

    @staticmethod
    def run(trash=None, count=5):
        TimePolicy.delete_by_time(trash, count)