from autodelete_time_policy import list_for_autodelete_by_time
from autodelete_size_policy import list_for_autodelete_by_size


def autodeleted_by_policy(trash=None):

    if trash.POLICY == 'time':
        list_for_del = list_for_autodelete_by_time(trash)
    elif trash.POLICY == 'size':
        list_for_del = list_for_autodelete_by_size(trash)

    for file_for_del in list_for_del:
        trash.remove_from_trash(file_for_del)