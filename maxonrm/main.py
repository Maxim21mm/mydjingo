import os
import json


def path_to_dict(path):
    d = {'name': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = [path_to_dict(os.path.join(path, x)) for x in os.listdir(path)]
    else:
        d['type'] = "file"
    return d


if __name__ == "__main__":
    print json.dumps(path_to_dict('/home/maxim/PycharmProjects/main'))
    config_path = '~/PycharmProjects'
    with open(os.path.join(os.path.expanduser(config_path), 'config.json'), 'wb') as json_config:
        json.dump(path_to_dict('/home/maxim/PycharmProjects/main'), json_config, indent=0)
