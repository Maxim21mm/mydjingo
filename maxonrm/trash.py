import os
import datetime
import shutil
import re
from maxonrm.ver import Checker
from maxonrm.autodelete import autodeleted_by_policy
from maxonrm.rej import run_del
from maxonrm.rej import run_rest
import tempfile
import multiprocessing


class Trash(object):

    def __init__(self, trash_path, max_size, size, policy, mode, date, restore_policy):

        # trash_path = '/home/maxim/PycharmProjects/TRASH'

        self.TRASH_PATH = trash_path
        self.INFO_PATH = self.TRASH_PATH + '/info'
        self.FILES_PATH = self.TRASH_PATH + '/files'
        self.MAX_SIZE = max_size
        self.POLICY = policy    # time or size
        self.DATE = date + ' 02:48:02.020848'
        self.SIZE = size
        self.SILENT_MOD = False
        self.DRY_RUN_MOD = False
        self.MODE = mode    # interactive, force
        self.RESTORE_POLICY = restore_policy     # replace, ask

        self.make_dir(self.TRASH_PATH)
        self.make_dir(self.INFO_PATH)
        self.make_dir(self.FILES_PATH)

        autodeleted_by_policy(self)

        Checker.update(self.TRASH_PATH)

    @staticmethod
    def make_dir(path):
        try:
            os.mkdir(path)
        except OSError:
            pass

    def dry_run_switcher(self):
        self.DRY_RUN_MOD = not self.DRY_RUN_MOD

    def delete_file(self, dir_path, file_name):

            path = os.path.join(dir_path, file_name)

            if os.path.lexists(path):
                if not Checker.trash_check(self.FILES_PATH, path):
                    if Checker.check_secure(file_name):
                        run_del(file_name, path, self)
                    else:
                        raise Exception("dir %s secure" % file_name)
                else:
                    raise Exception("file %s is already in trash or this dir secure" % file_name)
            else:
                raise IOError("Don't found file or directory %s" % file_name)

    def restore_file(self, file_name):
        old_path = os.path.join(self.FILES_PATH, file_name)
        if os.path.lexists(old_path):
            with open(os.path.join(self.INFO_PATH, file_name + ".file_info"), "r") as file_info:
                new_path = file_info.readline().strip("\n")
                # try:
                run_rest(file_name, new_path, old_path, self)
                # except:
                #     os.makedirs(os.path.split(new_path)[0])
                #     run_rest(file_name, new_path, old_path, self)

        else:
            raise IOError("file %s don't found" % file_name)

    def remove_from_trash(self, file_name):
        file_path = os.path.join(self.FILES_PATH, file_name)
        info_path = os.path.join(self.INFO_PATH, file_name + ".file_info")
        if os.path.lexists(file_path):
            if not self.DRY_RUN_MOD:
                if os.path.isdir(file_path):
                    shutil.rmtree(file_path)
                    os.remove(info_path)
                else:
                    os.remove(file_path)
                    os.remove(info_path)
        else:
            raise IOError("%s don't founded" % file_name)

    def clean_trash(self):
        # self.look_trash()
        if not self.DRY_RUN_MOD:
            shutil.rmtree(self.INFO_PATH)
            shutil.rmtree(self.FILES_PATH)
            self.make_dir(self.INFO_PATH)
            self.make_dir(self.FILES_PATH)

    def look_trash(self):
        files = os.listdir(self.INFO_PATH)
        for file_something in files:
            file_name = os.path.join(self.INFO_PATH, file_something)
            with open(file_name, "r") as file_info:
                file_path = file_info.readline()
            return ".".join(file_something.split(".")[:-1]), "\t", file_path

    def remove_by_re(self, path, re_ex, count):
        files_or_dirs = os.listdir(path)
        for file_or_dir in files_or_dirs:
            if re.search(re_ex, file_or_dir):
                try:
                    self.delete_file(path, file_or_dir)
                except:
                    pass
            else:
                if os.path.isdir(os.path.join(path, file_or_dir)):
                    count += 1

                    self.multi_remove_by_re(os.path.join(path, file_or_dir), re_ex, count)

    def multi_remove_by_re(self, path, re_ex, count):
        if count < 5:
            p = multiprocessing.Process(name=count, target=self.remove_by_re, args=(path, re_ex, count,))
            print(p.name)
            p.start()
        else:
            self.remove_by_re(path, re_ex, count)

    def multi_delete_file(self, files_list):
        tasks = multiprocessing.JoinableQueue()
        results = multiprocessing.Queue()

        if len(files_list) < 4:
            num_consumers = len(files_list)
        else:
            num_consumers = 4

        consumers = [Consumer(tasks, results)
                     for i in range(num_consumers)]
        for w in consumers:
            w.start()

        for i in files_list:
            print(i)
            folder, name = os.path.split(i)
            print(folder)
            print(name)
            tasks.put(Task(folder, name, self))

        for i in range(num_consumers):
            tasks.put(None)

        tasks.join()

        x = len(files_list)

        while x:
            result = results.get()
            if result == 1:
                return 1
            x -= 1
        return 0


class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                self.task_queue.task_done()
                break
            answer = next_task()
            self.task_queue.task_done()
            self.result_queue.put(answer)
        return


class Task(object):
    def __init__(self, path, name, trash):
        self.name = name
        self.path = path
        self.t = trash
        self.e = 0

    def __call__(self):
        try:
            self.t.delete_file(self.path, self.name)
        except:
            self.e = 1
        return self.e

    def __str__(self):
        return self.name


if __name__ == '__main__':
    t = Trash('/home/maxim/PycharmProjects/TRASH', 3, 3, 'size', 'force', '22', 'ask')
    # t.look_trash()
    f = ['/home/maxim/ww/ee/4', '/home/maxim/ww/ee/5', '/home/maxim/ww/ee/8']
    t.multi_delete_file(f)
