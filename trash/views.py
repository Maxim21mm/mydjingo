# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from .models import Trash
from .models import Task
from .taskforms import TaskForm
from .trashforms import TrashForm
from django.shortcuts import redirect
from .models import Regex
from .regexforms import RegexForm

from django.views.decorators.csrf import csrf_exempt


def trash_list(request):
    trashs = Trash.objects.all()
    return render(request, 'trash/trash_list.html', {'trashs': trashs})


def trash_detail(request, pk):
    # print(request.POST)
    # try:
    # print(request.POST['del'])
    #
    #     print(request.POST[u'del'])
    #     trash = get_object_or_404(Trash, pk=pk)
    #     file = request.POST.get('del')
    #     trash.remove_from_trash(file)
    #     return redirect('trash_detail', pk=trash.pk)

    trash = get_object_or_404(Trash, pk=pk)
    if request.method == "POST":

        if 'del_trash' in request.POST:
            trash.delete()
            return redirect('trash_list')

        if 'clean_trash' in request.POST:
            trash.clean_trash()
            t = Task(task_status='trash cleaning: completed', files='', trash_obj=trash)
            t.save()
            return redirect('trash_detail', pk=trash.pk)

        if 'rest' in request.POST:
            print(request.POST[u'rest'])
            file = request.POST.get('rest')
            try:
                trash.restore_file(file)
                t = Task(task_status='restore: completed', files=file, trash_obj=trash)
                t.save()
                return redirect('trash_detail', pk=trash.pk)
            except:
                t = Task(task_status='restore: error', files=file, trash_obj=trash)
                t.save()
                print 'Ooops'
                return redirect('task_list')

        if 'del' in request.POST:
            print(request.POST[u'del'])
            file = request.POST.get('del')
            trash.remove_from_trash(file)
            t = Task(task_status='delet e from trash: completed', files=file, trash_obj=trash)
            t.save()
            return redirect('trash_detail', pk=trash.pk)

    return render(request, 'trash/trash_detail.html', {'trash': trash})


def trash_new(request):
    if request.method == "POST":
        form = TrashForm(request.POST)
        if form.is_valid():
            trash = form.save(commit=False)
            trash.create()
            return redirect('trash_detail', pk=trash.pk)
    else:
        form = TrashForm()
    return render(request, 'trash/trash_edit.html', {'form': form})


def trash_edit(request, pk):
    trash = get_object_or_404(Trash, pk=pk)
    if request.method == "POST":
        form = TrashForm(request.POST, instance=trash)
        if form.is_valid():
            trash = form.save(commit=False)
            trash.save()
            return redirect('trash_detail', pk=trash.pk)
    else:
        form = TrashForm(instance=trash)
    return render(request, 'trash/trash_edit.html', {'form': form})


def task_list(request):
    tasks = reversed(Task.objects.all())
    return render(request, 'trash/task_list.html', {'tasks': tasks})


def task_detail(request, pk):
    task = get_object_or_404(Task, pk=pk)
    files = task.files.split(', ')
    if request.method == "POST":
        task.delete()
        return redirect('task_list')

    return render(request, 'trash/task_detail.html', {'files': files, 'task': task})


@csrf_exempt
def add_task(request):
    print(request.POST)
    data = dict(request.POST)
    print(data)
    global my_files
    my_files = ''
    for f in data.values().pop():
        my_files = my_files + ', ' + f
    print(my_files)
    return render(request, 'trash/trash_list.html')


def task_new(request):
    # if request.method == "POST":
    #     form = TaskForm(request.POST)
    #
    #     if form.is_valid():
    #
    #         trash_obj = Trash.objects.get(id=form.cleaned_data['trash'])
    #         files = form.cleaned_data['files']
    #         task = Task(trash_obj=trash_obj, files=files, task_status='ready')
    #
    #         if task.run() == 0:
    #             task.task_status = 'completed'
    #         else:
    #             task.task_status = 'error'
    #         task.save()
    #         return redirect('task_list')
    # else:
    #     form = TaskForm()
    # from trash.fileslist import json_tree as j
    # return render(request, 'trash/task_edit.html', {'form': form, 'json_tree': j()})

    if request.method == "POST":
        form = TaskForm(request.POST)
        # my_files = C()
        # if my_files is None:
        # print(request.POST)
        # data = dict(request.POST)
        # print(data)
        # my_files = ''
        # for f in data.values().pop():
        #     my_files = my_files + '+' + f
        # print(my_files)
        #     return render(request, 'trash/trash_list.html')
        #
        # print(my_files)
        if form.is_valid():
            # task = Task(proc=form.cleaned_data['proc'],
            #             files=form.cleaned_data['files'],
            #             trash_obj=Trash.objects.get(id=form.cleaned_data['trash_obj'])
            #             )
            # print(task.files)
            # print(add_task(request))
            # my_files = '+/home/maxim/ee/dimic3+/home/maxim/ee/maxic10'
            global my_files
            task = form.save(commit=False)
            print(task.files)
            print(my_files)
            task.files = my_files
            print(task.files)
            files_list = task.files.split(', ')
            print(files_list)
            if task.run() == 0:
                task.task_status = 'delete: completed'
            else:
                task.task_status = 'delete: error'
            task.save()
            return redirect('task_list')
    else:
        form = TaskForm()
    from trash.fileslist import json_tree as j
    return render(request, 'trash/task_edit.html', {'form': form, 'json_tree': j()})


def task_edit(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            return redirect('task_detail', pk=task.pk)
    else:
        form = TaskForm(instance=task)
    from trash.fileslist import json_tree as j
    return render(request, 'trash/task_edit.html', {'form': form, 'json_tree': j()})


def regex_new(request):
    if request.method == "POST":
        form = RegexForm(request.POST)
        if form.is_valid():
            regex = form.save(commit=False)
            regex.save()
            regex.start()
            t = Task(task_status='delet e by regex: completed', files='in dir {}. by regex {}'.format(regex.directory,
                                                                            regex.reg_ex), trash_obj=regex.trash_obj)
            t.save()
            return redirect('trash_list')
    else:
        form = RegexForm()
    return render(request, 'trash/regex_new.html', {'form': form})