from django import forms
from .models import Trash


class TrashForm(forms.ModelForm):
    class Meta:
        model = Trash
        exclude = ["tmp_trash"]
    # name = forms.CharField(max_length=50)
    # path = forms.CharField(max_length=500)
    # max_size = forms.IntegerField()
    # size = forms.IntegerField()
    # policy = forms.CharField(max_length=5)
    # mode = forms.CharField(max_length=15)
    # restore_policy = forms.CharField(max_length=10)
