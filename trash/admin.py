from django.contrib import admin
from .models import Trash, Task, Regex

admin.site.register(Trash)
admin.site.register(Task)
admin.site.register(Regex)
