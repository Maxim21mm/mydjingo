import os
import json

#
# def make_tree(path):
#     tree = dict(name=os.path.basename(path), children=[])
#     try:
#         lst = os.listdir(path)
#     except OSError:
#         pass
#     else:
#         for name in lst:
#             fn = os.path.join(path, name)
#             if os.path.isdir(fn):
#                 tree['children'].append(make_tree(fn))
#             else:
#                 tree['children'].append(dict(name=name))
#     return tree


def path_to_dict(path):
    d = {'text': os.path.basename(path), 'id': os.path.abspath(path)}
    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = [path_to_dict(os.path.join(path, x)) for x in os.listdir(path)]
    else:
        d['type'] = "file"
    return d


def json_tree():
    return json.dumps([path_to_dict(os.path.expanduser("/home/maxim/ee"))])
