from django import forms
from .models import Task
from .models import Trash


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['trash_obj']
    #     fields = '__all__'
    # trash_obj = forms.ChoiceField(
    #     choices=[(x.id, '{} ({})'.format(x.name, x.path)) for x in Trash.objects.all()]
    # )
    # proc = forms.IntegerField(min_value=1, initial=8, label='proc')
    # files = forms.Field(widget=forms.HiddenInput, required=False)

# class TaskForm(forms.Form):
#     trash_obj = forms.ChoiceField(
#         choices=[(x.id, '{} ({})'.format(x.name, x.path))
#                  for x in Trash.objects.all()]
#     )
#     files = forms.Field(widget=forms.HiddenInput, required=False)
