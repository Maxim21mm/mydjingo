from django.db import models
from django.utils import timezone
from maxonrm.trash import Trash as Trash_2
import os


class Trash(models.Model):
    name = models.CharField(max_length=50, default='new_trash')
    path = models.CharField(max_length=500, default='/home/maxim/qq')
    max_size = models.IntegerField(default=15000000)
    size = models.IntegerField(default=15000000)
    policy = models.CharField(max_length=5, default='time')
    date = timezone.now()
    mode = models.CharField(max_length=15, default='force')
    restore_policy = models.CharField(max_length=10, default='replace')
    # tmp_trash = Trash_2(os.path.join(path, name), max_size, size, self.policy, self.mode, '12',
    #                     restore_policy)

    def clean_trash(self):
        tmp_trash = Trash_2(os.path.join(self.path, self.name), self.max_size, self.size, self.policy, self.mode, '12',
                            self.restore_policy)
        tmp_trash.clean_trash()

    def files_list(self):
        return os.listdir(os.path.join(self.path, self.name)+'/files')

    def restore_file(self, file_name):
        tmp_trash = Trash_2(os.path.join(self.path, self.name), self.max_size, self.size, self.policy, self.mode, '12',
                            self.restore_policy)
        tmp_trash.restore_file(file_name)

    def remove_from_trash(self, file_name):
        tmp_trash = Trash_2(os.path.join(self.path, self.name), self.max_size, self.size, self.policy, self.mode, '12',
                            self.restore_policy)
        tmp_trash.remove_from_trash(file_name)

    def create(self):
        tmp_trash = Trash_2(os.path.join(self.path, self.name), self.max_size, self.size, self.policy, self.mode, '12',
                            self.restore_policy)
        try:
            os.mkdir(os.path.join(self.path, self.name))
        except OSError:
            pass
        self.save()

    def __str__(self):
        return self.name


class Task(models.Model):
    # STATUS = (
    #     (1, 'completed'),
    #     (2, 'error')
    # )
    # task_status = models.IntegerField(default=1)
    task_status = models.CharField(max_length=20)
    trash_obj = models.ForeignKey(Trash)
    files = models.TextField()

    def create(self):
        self.save()



    def run(self):
        tmp_trash = Trash_2(os.path.join(self.trash_obj.path, self.trash_obj.name), self.trash_obj.max_size,
                            self.trash_obj.size, self.trash_obj.policy, self.trash_obj.mode, '12',
                            self.trash_obj.restore_policy)
        print('files', self.files)
        files_list = self.files.split(', ')
        tmp_trash.look_trash()
        print('v run', files_list[1:])
        return tmp_trash.multi_delete_file(files_list[1:])

    def __str__(self):
        return self.task_status


class Regex(models.Model):
    trash_obj = models.ForeignKey(Trash)
    directory = models.CharField(max_length=500)
    reg_ex = models.CharField(max_length=50)

    def start(self):
        tmp_trash = Trash_2(os.path.join(self.trash_obj.path, self.trash_obj.name), self.trash_obj.max_size,
                            self.trash_obj.size, self.trash_obj.policy, self.trash_obj.mode, '12',
                            self.trash_obj.restore_policy)

        tmp_trash.multi_remove_by_re(self.directory, self.reg_ex, 0)
