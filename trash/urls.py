from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.trash_list, name='trash_list'),
    url(r'^trash/(?P<pk>[0-9]+)/$', views.trash_detail, name='trash_detail'),
    url(r'^trash/new/$', views.trash_new, name='trash_new'),
    url(r'^trash/(?P<pk>[0-9]+)/edit/$', views.trash_edit, name='trash_edit'),
    url(r'^task/(?P<pk>[0-9]+)/$', views.task_detail, name='task_detail'),
    url(r'^task/new/$', views.task_new, name='task_new'),
    url(r'^task/(?P<pk>[0-9]+)/edit/$', views.task_edit, name='task_edit'),
    url(r'^tasks/$', views.task_list, name='task_list'),
    url(r'^regex/$', views.regex_new, name='regex_new'),
    url(r'^task/new/add_task$', views.add_task, name='add_task'),
]